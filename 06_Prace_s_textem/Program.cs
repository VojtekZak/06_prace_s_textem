﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_Prace_s_textem
{
    class Program
    {
        static void Main(string[] args)
        {
            // Zkusme se podivat na praci s textem.
            // Prohlednete si program, jsou zde predpripraveny tri metody.
            // Doplnte tela metod nasledujicim zpusobem:

            // DejCelkovyPocetSlov
            //  - vraci celkovy pocet slov ve vstupnim parametru <originalniText>

            // DejPocetPismen
            //  - vraci celkovy pocet vyskytu pismene <pismeno> ve vstupnim parametru <originalniText>

            // DejPocetKonkretnichSlov
            //  - vraci celkovy pocet slov ve vstupnim parametru <originalniText>

            // Bonus: Napiste metodu, ktera vrati nejcasteji se vyskytujici pismeno v textu.

            string originalniText = "";
            originalniText += "Tak jsem si řek, že když mám takovej pech, ";
            originalniText += "přeskočím na druhej břeh a zkusím štěstí na pánech, ";
            originalniText += "což vyrazilo dech především těm, co jsem znal, ";
            originalniText += "snad proto v posledních dnech se drželi dál.";
            originalniText += "A já to nechápal a toužil po lásce ";
            originalniText += "a vroucím objetí, tak jako v pohádce. ";
            originalniText += "Stal jsem se obětí samoty v zajetí ";
            originalniText += "a že je ti to jedno, tak to mě celkem sebralo. ";

            int pocetSlov = DejCelkovyPocetSlov(originalniText);
            Console.WriteLine($"V textu je celkem {pocetSlov} slov.");

            char pismeno = 'b';
            int pocetPismen = DejPocetPismen(originalniText, pismeno);
            Console.WriteLine($"V textu je {pocetPismen} pismen \"{pismeno}\".");

            string slovo = "jsem";
            int vyskytSlov = DejPocetKonkretnichSlov(originalniText, slovo);
            Console.WriteLine($"V textu se slovo \"{slovo}\" vyskytuje celkem {vyskytSlov}krat.");

        }

        private static int DejPocetKonkretnichSlov(string originalniText, string slovo)
        {
            return 0;
        }

        private static int DejPocetPismen(string originalniText, char pismeno)
        {
            return 0;
        }

        private static int DejCelkovyPocetSlov(string originalniText)
        {
            return 0;
        }
    }
}
